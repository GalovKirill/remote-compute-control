﻿using System;
using System.Diagnostics;
using System.IO;
using WindowsInput;
using WindowsInput.Native;

namespace MediaFileOpeningVLC
{
    public class VLC
    {
        private Process _process;
        private readonly InputSimulator _simulator = new InputSimulator();
        public void Open(string path)
        {
            if (!path.StartsWith("http"))
            {
                if (string.IsNullOrEmpty(path))
                {
                    throw new ArgumentException($"{nameof(path)} is null or empty: {path}");
                }

                if (!File.Exists(path))
                {
                    throw new FileNotFoundException($"file: {path} not found");
                }
            }
            
            _process?.Kill();

            _process = Process.Start("vlc", $"-f {path}");
            if (_process == null)
            {
                throw new Exception("process not started");
            }
        }

        public void Pause()
        {
            if (ProcessNotStarted)
            {
                return;
            }
            _simulator.Keyboard.KeyDown(VirtualKeyCode.SPACE);
        }

        private bool ProcessNotStarted => _process == null;

        public void Left()
        {
            if (ProcessNotStarted)
            {
                return;
            }
            _simulator.Keyboard.KeyDown(VirtualKeyCode.LEFT);
        }
        
        public void Right()
        {
            if (ProcessNotStarted)
            {
                return;
            }
            _simulator.Keyboard.KeyDown(VirtualKeyCode.RIGHT);
        }
        
        
    }
}