using System.IO;
using System.Net;

namespace RemoteCommandHandler
{
    public static class WebRequestResponseExtensions
    {
        public static string ReadToEnd(this Stream stream)
        {
            using (stream)
            using (var streamReader = new StreamReader(stream))
            {
                return streamReader.ReadToEnd();
            }
        }


        public static void WriteAll(this Stream stream, string str)
        {
            using (stream)
            using (var streamWriter = new StreamWriter(stream))
            {
                streamWriter.WriteLine(str);
            }
        }

        public static WebRequest WriteRequestData(this WebRequest request, string str)
        {
            request.GetRequestStream().WriteAll(str);
            return request;
        }

        public static string ReadResponseData(this WebResponse response)
        {
            return response.GetResponseStream().ReadToEnd();
        }
    }
}