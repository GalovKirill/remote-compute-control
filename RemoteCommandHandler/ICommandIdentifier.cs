namespace RemoteCommandHandler
{
    public interface ICommandIdentifier
    {
        bool IsOpenFileCmd(string command, out string fileUri);

        bool IsWhitespacePress(string command);
        bool IsRightArrowPress(string command);
        bool IsLeftArrowPress(string command);
        bool IsPathsYoutubeCmd(string command);
    }
}