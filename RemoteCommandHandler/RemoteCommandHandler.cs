﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace RemoteCommandHandler
{
    public class RemoteCommandHandler
    {
        private readonly ICommandExecutor _commandExecutor;
        private readonly ICommandIdentifier _commandIdentifier;
        private readonly HttpListener _server = new HttpListener();
        private readonly IVideoStorage _storage;

        public RemoteCommandHandler(ICommandExecutor commandExecutor, ICommandIdentifier commandIdentifier, IVideoStorage storage)
        {
            _commandExecutor = commandExecutor;
            _commandIdentifier = commandIdentifier;
            _storage = storage;
        }

        private bool ListenToken { get; set; } = true;


        public void Start(int port)
        {
            _server.Prefixes.Add($"http://localhost:{port}/");
            _server.Start();
            Listen();
        }


        private async void Listen()
        {
            if (_server == null) return;

            while (ListenToken && _server.IsListening)
            {
                try
                {
                    HttpListenerContext httpListenerContext = await _server.GetContextAsync();
                    string input = httpListenerContext.Request.InputStream.ReadToEnd();
                    string output = await Handle(input);
                    using (HttpListenerResponse response = httpListenerContext.Response)
                    {
                        response.OutputStream.WriteAll(output);
                    }
                }
                catch
                {
                    // ignored
                }
            }
        }

        private Task<string> Handle(string input)
        {
            if (_commandIdentifier.IsOpenFileCmd(input, out string fileUri))
            {
                _commandExecutor.OpenVideo(fileUri);
                return Task.FromResult("OK");
            }
            else if (_commandIdentifier.IsPathsYoutubeCmd(input))
            {
                return _storage.GetPathsOnYoutube();
            }


            return Task.FromResult("NOT OK");
        }

        public void Stop()
        {
            ListenToken = false;
            _server.Stop();
        }
    }
}