namespace RemoteCommandHandler
{
    public interface ICommandExecutor
    {
        void OpenVideo(string uri);

        void RightKeyboard();

        void LeftKeyboard();

        void Whitespace();
    }
}