namespace RemoteCommandHandler
{
    public class CommandIdentifier : ICommandIdentifier
    {
        private const string OpenFile = "openfile ";
        private const string WhitespaceCmd = "whitespace";
        private const string LeftArrow = "left";
        private const string RightArrow = "right";
        private const string PathsOnYoutube = "paths yotube";

        public bool IsOpenFileCmd(string command, out string fileUri)
        {
            bool isOpenFileCmd = command.StartsWith(OpenFile);
            fileUri = isOpenFileCmd ? command.Remove(0, OpenFile.Length) : null;
            return isOpenFileCmd;
        }

        public bool IsWhitespacePress(string command)
        {
            return command == WhitespaceCmd;
        }

        public bool IsRightArrowPress(string command)
        {
            return command == RightArrow;
        }

        public bool IsLeftArrowPress(string command)
        {
            return command == LeftArrow;
        }

        public bool IsPathsYoutubeCmd(string command) => command == PathsOnYoutube;
    }
}