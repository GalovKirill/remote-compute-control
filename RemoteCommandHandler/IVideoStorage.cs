using System.Threading.Tasks;

namespace RemoteCommandHandler
{
    public interface IVideoStorage
    {
        Task<string> GetPathsOnDisk();

        Task<string> GetPathsOnYoutube();
    }
}