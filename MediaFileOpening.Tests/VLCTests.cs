﻿using System;
using System.Diagnostics;
using System.IO;
using MediaFileOpeningVLC;
using NUnit.Framework;

namespace MediaFileOpening.Tests
{
    [TestFixture]
    public class VLCTests
    {
        [TestCase("")]
        [TestCase((object)null)]
        public void Open_WhenPathIsUncorrected_ThrowArgumentException(string uncorrectedPath)
        {
            var vlc = new VLC();
            Assert.Catch<ArgumentException>(() => vlc.Open(uncorrectedPath));
        }

        [Test]
        public void Open_WhenFileNotExist_ThrowFileNotFoundException()
        {
            var vlc = new VLC();
            Assert.Catch<FileNotFoundException>(() => vlc.Open("this file is not exist!"));
        }

        [Test]
        public void Open_WhenVLCInstalled_ProcessStarted()
        {
            Process process = null;
            try
            {
                process = Process.Start("vlc");
            }
            catch (Exception e)
            {
                Assert.Fail(e.ToString());
            }
            Assert.AreNotEqual(null, process);
            process?.Kill();
        }
    }
}