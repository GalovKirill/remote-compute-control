﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace SomeThings
{
    internal class Program
    {
        public static void Main(string[] args)
        {
//            Process p = new Process();
//            ProcessStartInfo processStartInfo = p.StartInfo;
//            processStartInfo.FileName = @"vlc";
//            processStartInfo.Arguments = @"-f E:\Kirill\Downloads\IMG_0113.mp4";
////            processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
//            p.Start();
//            Thread.Sleep(2000);
//            IKeyboardSimulator i = new InputSimulator().Keyboard.KeyDown(VirtualKeyCode.SPACE);
//            p.WaitForExit();
//            TcpListener tcpListener = TcpListener.Create(8080);
//            //TcpClient acceptSocketAsync = await tcpListener.AcceptTcpClientAsync();
//            HttpListener a = new HttpListener();
//            a.GetContext().Response.

            HttpWebRequest req = WebRequest.CreateHttp("http://localhost:8888/");
            req.Method = WebRequestMethods.Http.Post;
            string readLine = null;
            using (Stream requestStream = req.GetRequestStream())
            {
                using (StreamWriter sw = new StreamWriter(requestStream))
                {
                    readLine = Console.ReadLine();
                    sw.WriteLine(readLine);
                }
            }

            WebResponse webResponse = req.GetResponse();
            Console.WriteLine(webResponse);
//            Task listen = Listen();
            Console.Read();
        }
        
        private static async Task Listen()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8888/");
            listener.Start();
            Console.WriteLine("Ожидание подключений...");
             
            while(true)
            {
                HttpListenerContext context = await listener.GetContextAsync();
                Console.WriteLine("есть контакт!");
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                StreamReader sr = new StreamReader(request.InputStream);
                Console.WriteLine(sr.ReadToEnd());
                sr.Close();
                response.Close();
            }
        }
    }
}