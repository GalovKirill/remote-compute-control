﻿using System.Net;
using NSubstitute;
using NUnit.Framework;

namespace RemoteCommandHandler.IntegrationTests
{
    [TestFixture]
    public class RemoteCommandHandlerTests
    {
        private RemoteCommandHandler Rlh { get; set; }

        private ICommandExecutor MockExecutor { get; set; }
        private ICommandIdentifier StubIdentifier { get; set; }
        private IVideoStorage MockStorage { get; set; }
        
        private HttpWebRequest Request { get; set; }

        [SetUp]
        public void Setup()
        {
            MockStorage = Substitute.For<IVideoStorage>();
            MockExecutor = Substitute.For<ICommandExecutor>();
            StubIdentifier = Substitute.For<ICommandIdentifier>();
            Rlh = new RemoteCommandHandler(MockExecutor, StubIdentifier, MockStorage);
            const int port = 8881;
            Rlh.Start(port);
            Request = WebRequest.CreateHttp($"http://localhost:{port}/");
            Request.Method = WebRequestMethods.Http.Post;
        }


        [TearDown]
        public void Teardown()
        {
            Rlh.Stop();
        }
        
        [Test]
        public void Listen_WhenListenedOpenFileCommand_ExecuteHer()
        {
            //arrange
            const string someFileUri = "someFileUri";
            StubIdentifier.IsOpenFileCmd(Arg.Any<string>(), out Arg.Any<string>())
                .Returns(info =>
                {
                    info[1] = someFileUri;
                    return true;
                });
            
            //act
            string response = null;
            using (WebResponse webResponse = Request.WriteRequestData("any").GetResponse())
            {
                response = webResponse.ReadResponseData();
            }
            
            //assert
            Assert.That(response, Does.StartWith("OK"));
            MockExecutor.Received().OpenVideo(Arg.Is(someFileUri));

        }
        
        
    }
}